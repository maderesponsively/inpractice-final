jQuery( document ).ready(function( $ ) {
	
		$.fn.slideFadeToggle  = function(speed, easing, callback) {
        return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
		};
		
		jQuery.fn.toggleText = function (value1, value2) {
				return this.each(function () {
						var $this = $(this),
								text = $this.html();
		 
						if (text.indexOf(value1) > -1)
								$this.html(text.replace(value1, value2));
						else
								$this.html(text.replace(value2, value1));
				});
		};
		
		
	
		$('#menuTrigger').tap(50, function() {
			$('#mainNav').slideToggle();
			$(this).toggleClass('open');
		});
		
		
		
		/* Modules Trigger */
		$('#e-modules .trigger').each(function() {
			$(this).tap(50, function() {		
				$(this).next('.e-modules').slideFadeToggle();
				$(this).toggleText('Close <i class="icon-open"></i>', 'Open <i class="icon-close"></i>');
			});
		});
		
});